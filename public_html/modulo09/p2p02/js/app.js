'use strict';
var map, lat, lng;
var ruta = [];
var btn_reset;
var btn_compres;

function guardar_ruta(la,ln) {
    if (typeof la !== 'undefined' && typeof ln !== 'undefined') {
        ruta.push([la,ln]);
    }
    if (localStorage) {
        localStorage.setItem('ruta',JSON.stringify(ruta));
    }
}

function limpiar_mapa(mapa) {
    mapa.removeMarkers();
    mapa.cleanRoute();
}

$(function(){
    btn_reset = $('#btn-reset');
    btn_compres = $('#btn-compres');

    btn_compres.on('click',function(){
        var ruta_aux = ruta;
        var final_ruta = ruta_aux.length-1;
        if (final_ruta) {
            ruta = [ruta_aux[0]];
            lat = ruta_aux[final_ruta][0];
            lng = ruta_aux[final_ruta][1];
        } else {
            ruta = [];
            lat = ruta_aux[0][0];
            lng = ruta_aux[0][1];
        }
        guardar_ruta(lat,lng);
        rehacerRuta();
    });

    btn_reset.on('click',function(){
        limpiar_mapa(map);
        ruta = [];
        navigator.geolocation.getCurrentPosition(
            function(position){
                guardar_ruta(position.coords.latitude,position.coords.longitude);
                lat = ruta[0][0];
                lng = ruta[0][1];
                map.addMarker({ lat: lat, lng: lng});
                map.setCenter(lat,lng);
            }
            ,function(error) {
                switch(error.code) {
                    case error.PERMISSION_DENIED:
                        disp.innerHTML = 'El usuario ha bloqueado la Geolocalización';
                        break;
                    case error.POSITION_UNAVAILABLE:
                        disp.innerHTML = 'Localización no disponible.';
                        break;
                    case error.TIMEOUT:
                        disp.innerHTML = 'Se ha sobrepasado el tiempo de espera.';
                        break;
                    case error.UNKNOWN_ERROR:
                        disp.innerHTML = 'Se ha producido un error no soportado.';
                        break;
                }
            }
        );

    })
    /**
     * Muestra la ruta entre dos puntos y guarda la nueva posición como última posición
     * @param  {object} e con el evento que se ha lanzado
     */
    function enlazarMarcador(e){
        dibujarRuta({lat:lat,lng:lng},{lat:e.latLng.lat(),lng:e.latLng.lng()})
        lat = e.latLng.lat();   // guarda coords para marca siguiente
        lng = e.latLng.lng();
        guardar_ruta(lat,lng);
        map.addMarker({ lat: lat, lng: lng});  // pone marcador en mapa
    };

    /**
     * Muestra la ruta entre dos puntos
     * @param  {object} orig con latitud y longitud del punto de origen
     * @param  {object} dest con latitud y longitud del punto de destino
     */
    function dibujarRuta(orig,dest) {
        map.drawRoute({
            origin: [orig.lat, orig.lng],
            destination: [dest.lat, dest.lng],
            travelMode: 'driving',
            strokeColor: '#000000',
            strokeOpacity: 0.6,
            strokeWeight: 5
        });
    }

    /**
     * Limpia el mapa y dibuja la ruta completa centrando la vista en el punto de origen.
     */
    function rehacerRuta() {
        limpiar_mapa(map);
        ruta.forEach(function(pos, index, array){
            if (index !== 0) {
                var post_ant = index - 1;
                var lat_ant = array[post_ant][0];
                var lng_ant = array[post_ant][1];
                dibujarRuta({lat:lat_ant,lng:lng_ant},{lat:pos[0],lng:pos[1]});
            }
            lat = pos[0];
            lng = pos[1];
            map.addMarker({ lat: lat, lng: lng});
        });
        map.setCenter(ruta[0][0],ruta[0][1]);
    }

    function geolocalizar(){
        GMaps.geolocate({
            success: function(position){
                var ruta_local;
                if (localStorage) {
                    ruta_local = JSON.parse(localStorage.getItem('ruta'));
                }
                if (!ruta_local) {
                    guardar_ruta(position.coords.latitude,position.coords.longitude);
                } else {
                    ruta = ruta_local;
                }
                lat = ruta[0][0];
                lng = ruta[0][1];
                map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
                    el: '#map',
                    lat: lat,
                    lng: lng,
                    click: enlazarMarcador,
                    tap: enlazarMarcador
                });
                rehacerRuta();
                btn_reset.removeClass('oculto');
                btn_compres.removeClass('oculto');
            },
            error: function(error) {
                alert('Geolocalización falla: '+error.message);
            },
            not_supported: function(){
                alert("Su navegador no soporta geolocalización");
            }
        });
    };

    geolocalizar();
});
