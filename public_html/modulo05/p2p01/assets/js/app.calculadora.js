'use strict';
$(function(){
    var calculadora = new Calculadora('num','oper_val','acc_val');
    $('.btn_oper').on('click',function(){
        calculadora.ejecutar_operacion($(this));
    });
    $('#calcular').on('click',function(){
        calculadora.calcular()
    });
    $('#num').on('click',function(){
        calculadora.vaciar()
    });
});
